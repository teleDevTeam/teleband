package ua.telesens.teleband

import android.app.ProgressDialog
import android.bluetooth.BluetoothDevice
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_log.*
import miband.ActionCallback
import miband.MiBand
import miband.model.BatteryInfo
import miband.model.LedColor
import miband.model.UserInfo
import miband.model.VibrationMode
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.util.*

class LogActivity : AppCompatActivity() {

    private val TAG = "==[mibandtest]=="

    val logList = mutableListOf<String>("test log")
    private var functionId: Int = -1
    lateinit var device: BluetoothDevice
    lateinit var adapter: ArrayAdapter<String>

    private var miband: MiBand? = ApiFunctionsDummy.miband


    private val Message_What_ShowLog = 1
    private val handler = object : Handler(Looper.getMainLooper()) {
        override fun handleMessage(m: Message) {
            when (m.what) {
                Message_What_ShowLog -> {
                    val text = m.obj as String
                    writeLog(text)
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log)

        functionId = intent.getIntExtra(INTENT_FUNCTION, -1)
        device = intent.getParcelableExtra(INTENT_DEVICE)

        val functionName = ApiFunctionsDummy.BUTTONS[functionId]
        lblLogTitle.text = String.format(getString(R.string.lbl_action_log), functionName)

        adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, logList)
        listLogs.adapter = adapter

        performAction()
    }

    private fun performAction() {
        when (functionId) {
            0 -> connectDevice()
            1 -> showServicesAndCharacteristics()
            2 -> readRssi()
            3 -> readBatteryInfo()
            4 -> setUserInfo()
            5 -> setHeartScanListener()
            6 -> miband!!.startHeartRateScan()
            7 -> miband!!.startVibration(VibrationMode.VIBRATION_WITH_LED)
            8 -> miband!!.startVibration(VibrationMode.VIBRATION_WITHOUT_LED)
            9 -> miband!!.startVibration(VibrationMode.VIBRATION_10_TIMES_WITH_LED)
            10 -> miband!!.stopVibration()
            11 -> setNotmalNotifyListener()
            12 -> setRealtimeStepsNotifyListener()
            13 -> miband!!.enableRealtimeStepsNotify()
            14 -> miband!!.disableRealtimeStepsNotify()
            15 -> miband!!.setLedColor(LedColor.ORANGE)
            16 -> miband!!.setLedColor(LedColor.BLUE)
            17 -> miband!!.setLedColor(LedColor.RED)
            18 -> miband!!.setLedColor(LedColor.GREEN)
            19 -> setSensorDataNotifyListener()
            20 -> miband!!.enableSensorDataNotify()
            21 -> miband!!.disableSensorDataNotify()
            22 -> pair()
        }
    }

    private fun showServicesAndCharacteristics() {
//        miband!!.showServicesAndCharacteristics()
        writeLog("Not supported for logging now")
    }

    private fun writeLog(message: String) {
        runOnUiThread({
            Log.d(TAG, message)
            logList.add(message)
            adapter.notifyDataSetChanged()
            adapter.notifyDataSetInvalidated()})
    }

    private fun setRealtimeStepsNotifyListener() {
        miband!!.setRealtimeStepsNotifyListener({ steps -> writeLog("RealtimeStepsNotifyListener:$steps") })
    }

    private fun setNotmalNotifyListener() {
        miband!!.setNormalNotifyListener({ data -> writeLog("NormalNotifyListener:" + Arrays.toString(data)) })
    }

    private fun setHeartScanListener() {
        miband!!.setHeartRateScanListener({ heartRate -> writeLog("heart rate: $heartRate") })
    }

    private fun pair() {
        miband!!.pair(object : ActionCallback {

            override fun onSuccess(data: Any?) {
                writeLog("paired successfully")
            }

            override fun onFail(errorCode: Int, msg: String) {
                writeLog("pair failed")
            }
        })
    }

    private fun setSensorDataNotifyListener() {
        miband!!.setSensorDataNotifyListener({ data ->
            ByteBuffer.wrap(data).order(ByteOrder.LITTLE_ENDIAN)
            var i = 0

            val index = (data[i++].toInt() and 0xFF) or (data[i++].toInt() and 0xFF shl 8)
            val d1 = (data[i++].toInt() and 0xFF) or (data[i++].toInt() and 0xFF shl 8)
            val d2 = (data[i++].toInt() and 0xFF) or (data[i++].toInt() and 0xFF shl 8)
            val d3 = (data[i++].toInt() and 0xFF) or (data[i++].toInt() and 0xFF shl 8)

            val m = Message()
            m.what = Message_What_ShowLog
            m.obj = index.toString() + "," + d1 + "," + d2 + "," + d3

            handler.sendMessage(m)
        })
    }

    private fun setUserInfo() {
        val userInfo = UserInfo(20271234, 1, 32, 160, 40, "1哈哈", 0)
        writeLog("setUserInfo:" + userInfo.toString() + ",data:" + Arrays.toString(userInfo.getBytes(miband!!.device.address)))
        miband!!.setUserInfo(userInfo)
    }

    private fun readBatteryInfo() {
        miband!!.getBatteryInfo(object : ActionCallback {

            override fun onSuccess(data: Any?) {
                val info = data as BatteryInfo
                writeLog(info.toString())
            }

            override fun onFail(errorCode: Int, msg: String) {
                writeLog("battery info retrieval failed")
            }
        })
    }

    private fun readRssi() {
        miband!!.readRssi(object : ActionCallback {

            override fun onSuccess(data: Any?) {
                writeLog("rssi:" + data as Int)
            }

            override fun onFail(errorCode: Int, msg: String) {
                writeLog("readRssi operation failed")
            }
        })
    }

    private fun connectDevice() {
        val pd = ProgressDialog.show(this@LogActivity, "", "Connecting, please wait")
        miband!!.connect(device, object : ActionCallback {

            override fun onSuccess(data: Any?) {
                pd.dismiss()
                writeLog("Connected")
                miband!!.startVibration(VibrationMode.VIBRATION_WITHOUT_LED)

                miband!!.setDisconnectedListener({
                    writeLog("Disconnected")
                })
            }

            override fun onFail(errorCode: Int, msg: String) {
                pd.dismiss()
                writeLog("connect failed, code:$errorCode,mgs:$msg")
            }
        })
    }

    companion object {

        private val INTENT_FUNCTION = "user_id"
        private val INTENT_DEVICE = "bluetooth_device"

        fun newIntent(context: Context, function: Int, device: BluetoothDevice): Intent {
            val intent = Intent(context, LogActivity::class.java)
            intent.putExtra(INTENT_FUNCTION, function)
            intent.putExtra(INTENT_DEVICE, device)
            return intent
        }
    }
}
