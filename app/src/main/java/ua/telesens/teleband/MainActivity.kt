package ua.telesens.teleband

import android.app.ProgressDialog
import android.bluetooth.BluetoothDevice
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.AdapterView
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_main.*
import miband.ActionCallback
import miband.MiBand
import miband.listeners.RealtimeStepsNotifyListener
import miband.model.BatteryInfo
import miband.model.LedColor
import miband.model.UserInfo
import miband.model.VibrationMode
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.util.*
import kotlin.experimental.and
import kotlin.experimental.or

class MainActivity : AppCompatActivity() {
    private val TAG = "==[mibandtest]=="

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val intent = this.intent
        val device = intent.getParcelableExtra<BluetoothDevice>("device")



        listAvailableActions.adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, ApiFunctionsDummy.BUTTONS)
        listAvailableActions.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->

            if(position == 0){

            }
            val intent = LogActivity.newIntent(this@MainActivity, position, device)
            startActivity(intent)
        }

        ApiFunctionsDummy.miband = MiBand(this)

    }
}
