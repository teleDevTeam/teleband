package ua.telesens.teleband

class ApiFunction{
    var code: Int
    var name: String

    constructor(code: Int, name: String){
        this.code = code
        this.name = name
    }
}