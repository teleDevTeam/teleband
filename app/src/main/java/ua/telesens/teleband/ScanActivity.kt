package ua.telesens.teleband

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.content.Intent
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.util.Log
import android.widget.AdapterView.OnItemClickListener
import android.widget.ArrayAdapter
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_scan.*
import miband.MiBand
import java.util.*


class ScanActivity : Activity() {


    internal var devices = HashMap<String, BluetoothDevice>()

    private var scanCallback: ScanCallback? = null
    private var leScanCallback: BluetoothAdapter.LeScanCallback? = null

    lateinit var adapter: ArrayAdapter<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scan)



        adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, ArrayList<String>())

        initCallback()


        startScanButton.setOnClickListener {
            Log.d(TAG, "Scanning for nearby bluetooth devices")
            startScan()
        }

        stopScanButton.setOnClickListener {
            Log.d(TAG, "Stopping scanning")
            stopScan()
        }

        scannedDevicesList.adapter = adapter
        scannedDevicesList.onItemClickListener = OnItemClickListener { parent, view, position, id ->
            val item = (view as TextView).text.toString()
            if (devices.containsKey(item)) {

                Log.d(TAG, "Stopping scanning")
                stopScan()

                val device = devices[item]
                val intent = Intent()
                intent.putExtra("device", device)
                intent.setClass(this@ScanActivity, MainActivity::class.java)
                this@ScanActivity.startActivity(intent)
                this@ScanActivity.finish()
            }
        }

    }

    private fun initCallback() {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            initCallback21()
        } else {
            initCallback18_20()
        }
    }

    private fun startScan(){
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            MiBand.startScan(scanCallback)
        } else {
            MiBand.startScan(leScanCallback)
        }
    }

    private fun stopScan(){
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            MiBand.stopScan(scanCallback)
        } else {
            MiBand.stopScan(leScanCallback)
        }
    }

    @RequiresApi(21)
    private fun initCallback21() {
        scanCallback = object : ScanCallback() {
            override fun onScanResult(callbackType: Int, result: ScanResult) {
                val device = result.device
                Log.d(TAG,
                        "Found device: name:" + device.name + ",uuid:"
                                + device.uuids + ",add:"
                                + device.address + ",type:"
                                + device.type + ",bondState:"
                                + device.bondState + ",rssi:" + result.rssi)

                val item = device.name + "|" + device.address
                if (!devices.containsKey(item)) {
                    devices[item] = device
                    adapter.add(item)
                }

            }
        }
    }

    private fun initCallback18_20() {
        leScanCallback = BluetoothAdapter.LeScanCallback { bluetoothDevice, rssi, scanRecord ->
            runOnUiThread({
                Log.d(TAG,
                        "Found device: name:" + bluetoothDevice.name + ",uuid:"
                                + bluetoothDevice.uuids + ",add:"
                                + bluetoothDevice.address + ",type:"
                                + bluetoothDevice.type + ",bondState:"
                                + bluetoothDevice.bondState + ",rssi:" + rssi)

                val item = bluetoothDevice.name + "|" + bluetoothDevice.address
                if (!devices.containsKey(item)) {
                    devices[item] = bluetoothDevice
                    adapter.add(item)
                }
            })
        }
    }

    companion object {
        private const val TAG = "==[mibandtest]=="
    }
}
