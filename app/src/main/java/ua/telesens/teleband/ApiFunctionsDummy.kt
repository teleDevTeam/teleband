package ua.telesens.teleband

import miband.MiBand

class ApiFunctionsDummy{
    var functionList: MutableList<ApiFunction>

    constructor(){

        functionList = mutableListOf()
        functionList.add(ApiFunction(0, "Connect"))
    }
    companion object {

        var miband: MiBand? = null

        val BUTTONS = arrayOf(
                "Connect",
                "showServicesAndCharacteristics",
                "read_rssi",
                "battery_info",
                "setUserInfo",
                "setHeartRateNotifyListener",
                "startHeartRateScan",
                "miband.startVibration(VibrationMode.VIBRATION_WITH_LED);",
                "miband.startVibration(VibrationMode.VIBRATION_WITHOUT_LED);",
                "miband.startVibration(VibrationMode.VIBRATION_10_TIMES_WITH_LED);",
                "stopVibration",
                "setNormalNotifyListener",
                "setRealtimeStepsNotifyListener",
                "enableRealtimeStepsNotify",
                "disableRealtimeStepsNotify",
                "miband.setLedColor(LedColor.ORANGE);",
                "miband.setLedColor(LedColor.BLUE);",
                "miband.setLedColor(LedColor.RED);",
                "miband.setLedColor(LedColor.GREEN);",
                "setSensorDataNotifyListener",
                "enableSensorDataNotify",
                "disableSensorDataNotify",
                "pair")
    }

}