package miband.listeners;

public interface HeartRateNotifyListener {
    public void onNotify(int heartRate);
}
