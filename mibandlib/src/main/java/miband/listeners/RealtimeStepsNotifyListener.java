package miband.listeners;

public interface RealtimeStepsNotifyListener {
    public void onNotify(int steps);
}
